package com.cyrekdigital.flutterpushplugin

import android.widget.Toast
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar

class FlutterPushPlugin(private val registrar: Registrar) : MethodCallHandler {
  companion object {
    @JvmStatic
    fun registerWith(registrar: Registrar) {
      val channel = MethodChannel(registrar.messenger(), "flutter_push_plugin")
      channel.setMethodCallHandler(FlutterPushPlugin(registrar))
    }
  }

  override fun onMethodCall(call: MethodCall, result: Result) {

    if (call.method == "getPlatformVersion") {
      result.success("Android ${android.os.Build.VERSION.RELEASE}")
    }
    else if (call.method == "showToast") {
      val message: String? = call.argument<String>("message")
      Toast.makeText(registrar.context(), message ?: "", Toast.LENGTH_SHORT).show()
    }
    else if (call.method == "showNotification") {
      val title: String = call.argument<String>("title") ?: ""
      val body: String = call.argument<String>("body") ?: ""
      displayNotification(title, body)
    }
    else {
      result.notImplemented()
    }
  }

  private fun displayNotification(title: String, body: String){
    val notificationManager = NotificationManager
    notificationManager.triggerNotification(registrar.context(), title, body)
  }

}


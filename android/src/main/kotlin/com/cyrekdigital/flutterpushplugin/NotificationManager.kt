package com.cyrekdigital.flutterpushplugin

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat


object NotificationManager {

    private val CHANNEL_ID = "YOUR_CHANNEL_ID"
    private val CHANNEL_NAME = "Your human readable notification channel name"
    private val CHANNEL_DESCRIPTION = "description"

    fun triggerNotification(context: Context, title: String, body: String){

        val mContext: Context = context
        createNotificationChannel(mContext)
        createNotification(mContext,title, body)
    }


    private fun getNotificationManager(context: Context): NotificationManagerCompat {
        return NotificationManagerCompat.from(context)
    }

    private fun createNotificationChannel(context: Context) {
        val mContext: Context = context
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelId = CHANNEL_ID
            val name = CHANNEL_NAME
            val description = CHANNEL_DESCRIPTION
            val importance = android.app.NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channelId, name, importance)
            channel.description = description
            val notificationManager = mContext.getSystemService(android.app.NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun createNotificationCompatBuilder(context: Context): NotificationCompat.Builder {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return NotificationCompat.Builder(context, CHANNEL_ID)
        } else {
            return NotificationCompat.Builder(context)
        }
    }

    private fun createNotification(context: Context, title: String, body: String){

        val mContext: Context = context
        val intent = Intent(mContext, getMainActivityClass(mContext))
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        val pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        val notificationBuilder = createNotificationCompatBuilder(mContext)

        notificationBuilder.setContentTitle(title)
        notificationBuilder.setContentText(body)
        notificationBuilder.setStyle(NotificationCompat.BigTextStyle().bigText(body))
        notificationBuilder.setAutoCancel(true)
        notificationBuilder.setDefaults(Notification.DEFAULT_SOUND)
        notificationBuilder.setVibrate(longArrayOf(200, 500, 200, 500, 200, 500, 200 ))
        notificationBuilder.setSmallIcon(getMainAppIcon(mContext))
        notificationBuilder.setContentIntent(pendingIntent)

        val notificationManager = getNotificationManager(mContext)
        notificationManager.notify(System.currentTimeMillis().toInt(), notificationBuilder.build())
    }

    private fun getMainActivityClass(context: Context): Class<*>? {
        val packageName = context.packageName
        val launchIntent = context.packageManager.getLaunchIntentForPackage(packageName)
        val className = launchIntent!!.component!!.className
        try {
            return Class.forName(className)
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
            return null
        }
    }

    private fun getMainAppIcon(context: Context): Int {
        val mContext: Context = context
        val packageName = mContext.packageName
        return mContext.packageManager.getApplicationInfo(packageName, 0).icon
    }
}

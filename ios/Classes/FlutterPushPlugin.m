#import "FlutterPushPlugin.h"
#import <flutter_push_plugin/flutter_push_plugin-Swift.h>

@implementation FlutterPushPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterPushPlugin registerWithRegistrar:registrar];
}
@end

import Flutter
import UIKit
import UserNotifications

public class SwiftFlutterPushPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "flutter_push_plugin", binaryMessenger: registrar.messenger())
    let instance = SwiftFlutterPushPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
    
  }

    
  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    
    result("iOS " + UIDevice.current.systemVersion)
    var map = call.arguments as? Dictionary<String, String>
    
    if call.method == "showToast" {
        let message = map?["message"]
        
        let alert = UIAlertController(title: "awesome iOS", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    else if call.method == "showNotification" {
        guard let unwrappedMap = map else { return }
        presentModal(map: unwrappedMap)
    }
  }
    
    private func presentModal(map: Dictionary<String, String>) {
        let modalViewController = DisplayNotificationClass()
        modalViewController.map = map
        modalViewController.modalPresentationStyle = .overCurrentContext
       UIApplication.shared.keyWindow?.rootViewController?.present(modalViewController, animated: true, completion: nil)
    }
    
}

class DisplayNotificationClass: UIViewController {
    
    var map: Dictionary<String, String>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        displayNotification()
    }
    
    private func setupViews(){
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        } else {
            // Fallback on earlier versions
        }
    }
    
    @objc func viewDissmis(){
        self.dismiss(animated: false, completion: nil)
    }
    
      func displayNotification(){
        
        let title = map?["title"]
        let body = map?["body"]
        
        let notification = UILocalNotification()
        notification.fireDate = NSDate(timeIntervalSinceNow: 0) as Date
        notification.alertBody = body ?? ""
        if #available(iOS 8.2, *) {
            notification.alertTitle = title ?? ""
        } else {
            // Fallback on earlier versions
            print("error: iOS version is lower than 8.2, Notification title won't be displayed.")
        }
        notification.alertAction = "open"
        notification.hasAction = true
        notification.userInfo = ["UUID": "reminderID" ]
        UIApplication.shared.scheduleLocalNotification(notification)
    }
}


@available(iOS 10.0, *)
extension DisplayNotificationClass: UNUserNotificationCenterDelegate {

    //for displaying notification when app is in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
        viewDissmis()
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
//            self.viewDissmis()
//        }
    }

    // For handling tap and user actions
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("user tap notification")
        completionHandler()
    }

}

import 'dart:async';

import 'package:flutter/services.dart';

class FlutterPushPlugin {
  static const MethodChannel _channel =
      const MethodChannel('flutter_push_plugin');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<Null> showToast(String message) async {
    await _channel.invokeMethod("showToast", {"message": message});
    return null;
  }

  static Future<Null> displayNotification(String title, String body) async {
    await _channel.invokeMethod("showNotification", {"title": title, "body": body});
    return null;
  }
}
